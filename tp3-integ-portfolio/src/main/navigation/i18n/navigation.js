const translations = {
    fr: {
      navigation: {
        myProjects: "Mes projets",
        presentation: "Présentation",
        contact: "Contact",
        french: "Français",
        english: "Anglais"

      },
    },
    en: {
      navigation: {
        myProjects: "My projects",
        presentation: "Presentation",
        contact: "Contact",
        french: "French",
        english: "English",
      },
    },
  };
  
export default translations;