import i18next from "i18next";
import { Route, Switch, Link, BrowserRouter as Router } from "react-router-dom";
import PresentationScreen from "../../presentation/PresentationScreen";
import ProjectsScreen from "../../projects/ProjectsScreen";
import { useTranslation } from "react-i18next";
import ContactScreen from "../../contact/ContactScreen";

const AppNavigator = () => {
    
    const change = (option) => {
        localStorage.setItem('lang', option.target.value)
        window.location.reload();
    }

    const lang = localStorage.getItem('lang') || 'fr';
    i18next.changeLanguage(lang);

    const { t } = useTranslation("navigation");

    return (
        <Router>
        <div>
        <select className="selected_lang" onChange={change} value={lang}>
            <option value="fr">{t("french")}</option>
            <option value="en">{t("english")}</option>
        </select>

        </div>
        <nav className="nav">
            <div className="nav-div">
                <ul>
                    <li>
                        <Link to="/">
                            {t("presentation")}
                        </Link>
                    </li>

                    <li >
                        <Link to="/Projects">
                            {t("myProjects")}
                        </Link>
                    </li>

                    <li>
                        <Link to="/Contact">
                            {t("contact")}
                        </Link>
                    </li>
                </ul>
            </div>
        </nav>

        <Switch>
        <Route exact path="/">
            <PresentationScreen/>
        </Route>

        <Route exact path="/projects">
            <ProjectsScreen/>
        </Route>

        <Route exact path="/contact">
            <ContactScreen/>
        </Route>

        </Switch>
    </Router>
  );
};

export default AppNavigator;