import merge from "deepmerge";
import project from "../../projects/i18n/project";
import navigation from "../navigation/i18n/navigation";
import presentation from "../../presentation/i18n/presentation";
import contact from "../../contact/i18n/contact";

export default merge.all([project, navigation, presentation, contact]);