const translations = {
  fr: {
    project: {
      myProject: "Mes projets",
      screenMode: "Changer la couleur",
      myIntegrationProject: "Projet d'intégration",
      myIntegrationText: "Nous devions faire un quiz et l'utilisateur devait répondre aux questions, dépendamment de ses réponses, il y a un tableau de statistiques à la fin, s'il à réussit ou échoué le test.",
      myAnimationProject: "Projet d'animation",
      myAnimationText: "Nous devions faire une animation pour une compagnie fictive qui vend des boîtes de carton.",
      otherIntegrationText: "Nous devions faire l'interface d'un site web pour une compagnie fictive offrant des services dans le domaine du web."
    },
  },
  en: {
    project: {
      myProject: "My projects",
      screenMode: "Change the color",
      myIntegrationProject: "Integration project",
      myIntegrationText: "We had to do a quiz and the user had to answer the questions, depending on their answers, there is a statistics table at the end, if they passed or failed the test.",
      myAnimationProject: "Animation project",
      myAnimationText: "We had to do an animation for a fictitious company that sells cardboard boxes.",
      otherIntegrationText: "We had to do the interface of a website for a fictitious company offering web services."
    },
  },
};

export default translations;