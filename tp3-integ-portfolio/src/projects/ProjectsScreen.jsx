/* eslint-disable react/jsx-no-comment-textnodes */
//import React from "react";
import React, { useState } from "react";
import image1 from "../images/image1.jpg";
import image2 from "../images/image2.jpg";
import image3 from "../images/image3.jpg";
import { useTranslation } from "react-i18next";
import { themes } from "../main/theme/Theme";

function ProjectsScreen() {

    // Traduction
    const { t } = useTranslation("project");

    // Thème de couleur par defaut light
    const [theme, setTheme] = useState(themes.light);
    const toggleTheme = () => theme === themes.dark ? setTheme(themes.light) : setTheme(themes.dark);
  
    return (
    <>
      <button className="lumino" onClick={toggleTheme}>{t("screenMode")}</button>
      <div style={theme}>
      <h1>{t("myProject")}</h1>
      <div className="project">
            <img src={image1} alt="project number one." width="800px"/>
            <div>
              <h4 className="project_name">
                <a
                  href="https://gitlab.com/laurie_boilard/tp3integration.git"
                  target="_blank"
                  rel="noreferrer">
                  {t("myIntegrationProject")}
                </a>
              </h4>
              <h5>Quiz - Jquery</h5>
              <p>{t("myIntegrationText")}</p>
            </div>
        </div>

        <div className="project">
            <img src={image2} alt="project number two." width="800px"/>
            <div>
              <h4 className="project_name">
                <a
                  href="https://gitlab.com/laurie_boilard/Tp4-animation.git"
                  target="_blank"
                  rel="noreferrer">
                {t("myAnimationProject")}
                </a>
              </h4>
              <h5>Animation Css / Animate.js</h5>
              <p>{t("myAnimationText")}</p>
            </div>
        </div>


        <div className="project">
            <img src={image3}alt="project number three." width="800px" />
            <div>
              <h4 className="project_name">
                <a
                  href="https://gitlab.com/laurie_boilard/tp3-technique_integration.git"
                  target="_blank"
                  rel="noreferrer">
                  {t("myIntegrationProject")}
                </a>
              </h4>
              <h5> CSS / HTML</h5>
              <p>{t("otherIntegrationText")}</p>
            </div>
          </div>
        </div>
    </>
  );
}
export default ProjectsScreen;
