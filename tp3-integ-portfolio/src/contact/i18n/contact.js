const translations = {
  fr: {
    project: {
      contactMe: "Contactez-moi !",
      screenMode: "Changer la couleur",
      phone: "Téléphone",
      email: "Courriel",
      question: "Vous avez des questions ? Contactez-moi !"
    },
  },
  en: {
    project: {
      contactMe: "Contact",
      screenMode: "Change the color",
      phone: "Phone",
      email: "Email",
      question: "Any question ? Contact-me !" 
    },
  },
};

export default translations;