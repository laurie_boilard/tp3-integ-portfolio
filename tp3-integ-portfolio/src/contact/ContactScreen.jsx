/* eslint-disable react/jsx-no-comment-textnodes */
//import React from "react";
import React, { useState } from "react"
import { useTranslation } from "react-i18next";
import { themes } from "../main/theme/Theme";

function ContactScreen() {

    // Traduction
    const { t } = useTranslation("project");

    // Thème de couleur par defaut light
    const [theme, setTheme] = useState(themes.light);
    const toggleTheme = () => theme === themes.dark ? setTheme(themes.light) : setTheme(themes.dark);
  
    return (
    <>
        <button className="lumino" onClick={toggleTheme}>{t("screenMode")}</button>
        <div style={theme}>
        <h1>{t("contactMe")}</h1>
        <div className="contact">
            <h4>{t('question')}</h4>
            <div>
                <p>{t('phone')}</p>
                <h5 className="info"> 581-305-6663</h5>
            </div>
            <div>
                <p>{t('email')}</p>
                <h5 className="info">lboilard@outlook.com</h5>
            </div>

            </div>
        </div>
    </>
  );
}
export default ContactScreen;
