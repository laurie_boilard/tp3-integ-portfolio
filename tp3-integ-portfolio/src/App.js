import { I18nextProvider } from 'react-i18next';
import i18n from "./main/i18n/index.js";
import AppNavigator from './main/navigation/AppNavigator';
import { BrowserRouter as Router } from 'react-router-dom';
import "./App.css";


function App() {
  return (
    <div>
    <I18nextProvider i18n={i18n}>
      <Router>

        <AppNavigator/>
      </Router>
    </I18nextProvider>
  </div>
  );
};

export default App;
