const translations = {
  fr: {
    presentation: {
      myPresentation: "Qui suis-je ?",
      screenMode: "Changer la couleur",
      aboutMe: "À propos de moi",
      textAboutMe: "Mon nom est Laurie Boilard. J'étudie présentement au cégep Garneau où je fais un AEC en développement web. J'ai également en main un diplôme d'études professionnelles en infographie. J'adore le graphisme, étant créatif de nature, j'adore pouvoir mettre mes talents à profits.",
    },
  },
  en: {
    presentation: {
      myPresentation: "Who am i ?",
      screenMode: "Change the color",
      aboutMe: "About me",
      textAboutMe: "My name is Laurie Boilard. I am currently studying at Cégep Garneau where I am doing an AEC in web development. I also have a professional studies diploma in computer graphics. I love graphics, being creative by nature, I love being able to put my talents to use.",
    },
  },
};

export default translations;