//import React from "react"
import pictureOfme from "../images/moi.jpg"
import React, { useState } from "react"
import { useTranslation } from "react-i18next";
import { themes } from "../main/theme/Theme";

const PresentationScreen = () => {

    // Traduction
    const { t } = useTranslation("presentation");

    // Thème de couleur par defaut light
    const [theme, setTheme] = useState(themes.light);
    const toggleTheme = () => theme === themes.dark ? setTheme(themes.light) : setTheme(themes.dark);

    return (
        <>
        <button className="lumino" onClick={toggleTheme}>{t("screenMode")}</button>
        <div style={theme} >
            <h1 style={theme}>{t("myPresentation")}</h1>
            <div className="grid-pres">
                <img src={pictureOfme} alt="project number two." width="500px"/>
                <div>
                    <h4>{t("aboutMe")}</h4>
                    <p >{t("textAboutMe")}</p>
                </div>
            </div>
        </div>
        </>
    )
}

export default PresentationScreen;